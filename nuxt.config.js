export default {
  srcDir: 'src',
  head: {
    title: 'qtc-exchange-web',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  css: ['@/assets/scss/main.scss', '@fortawesome/fontawesome-free/css/all.css'],
  components: true,
  buildModules: ['@nuxtjs/eslint-module'],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/style-resources',
  ],
  plugins: ['./plugins/axios.js'],
  build: {
    modules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
  env: {
    apiUrl: process.env.API_URL,
  },
  bootstrapVue: {
    bootstrapCSS: true,
    bootstrapVueCSS: true,
  },
  styleResources: {
    scss: '@/assets/scss/_variables.scss',
  },
  server: {
    host: '0.0.0.0',
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
  fontawesome: {
    icons: {
      solid: true,
      regular: false,
      brands: false,
    },
  },
  axios: {
    baseURL: process.env.API_URL,
    credentials: false,
    timeout: 10000,
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: 'auth/sign-in',
            method: 'post',
            propertyName: 'access_token',
          },
          user: {
            url: 'auth/verify-token',
            method: 'get',
            propertyName: 'user',
          },
          logout: false,
        },
      },
    },
    token: {
      property: 'access_token',
    },
    redirect: {
      login: '/sign-in',
      logout: '/contents',
      home: '/contents',
    },
  },
}
