export const state = () => ({
  language: 'EN',
})

export const mutations = {
  setLanguage(state, language) {
    state.language = language
  },
}
