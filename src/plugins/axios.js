export default function ({ $axios }) {
  $axios.onResponse((res) => {
    return res
  })
  $axios.onError((error) => {
    return Promise.reject(error.response.data)
  })
}
