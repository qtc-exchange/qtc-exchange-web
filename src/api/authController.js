export function authController($axios) {
  const baseRoute = '/auth'
  return {
    signIn: async (body) => {
      const response = await $axios.post(`${baseRoute}/sign-in`, body)
      return response.data
    },
  }
}
