export function newsController($axios) {
  const baseRoute = '/news'
  return {
    search: async (params) => {
      const queryParams = new URLSearchParams(params).toString()
      const response = await $axios.get(`${baseRoute}?${queryParams}`)
      return response.data
    },
  }
}
