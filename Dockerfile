# STATE BUILD
FROM node:18-alpine AS builder

WORKDIR /app

ADD package.json ./
RUN yarn install

ADD . ./
RUN yarn build

RUN yarn --frozen-lockfile --non-interactive --production

# STATE PROD
FROM node:18-alpine AS production
WORKDIR /app

ADD package.json ./
ADD nuxt.config.js ./
ADD jsconfig.json ./

COPY --from=builder ./app/node_modules ./node_modules/
COPY --from=builder ./app/.nuxt ./.nuxt/
COPY --from=builder ./app/src/static ./src/static/
COPY --from=builder ./app/src/store ./src/store/

EXPOSE 3000
CMD ["yarn", "start", "-p", "3000"]
